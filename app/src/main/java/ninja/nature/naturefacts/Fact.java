package ninja.nature.naturefacts;

/**
 * Created by apgarm on 7/14/2017.
 */

public class Fact {
    private String id = "0";
    private String imageUrl = "imageUrl not initialized";
    private String text = "text not initialized";

    public Fact() {
        // Need this
    }

    public Fact(String id, String imageUrl, String text) {
        this.id = id;
        this.imageUrl = imageUrl;
        this.text = text;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}

package ninja.nature.naturefacts;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by apgarm on 7/14/2017.
 */

public class BackgroundFactLoader extends AsyncTask<String, Void, Bitmap> {
    private static Map<String, Bitmap> cache = new HashMap<String, Bitmap>();
    private ImageView bmImage;
    private TextView txtView;
    private Fact fact;
    private ProgressDialog dialog;
    private ProgressBar spinner;

    public BackgroundFactLoader(Context context, Fact fact, final ImageView bmImage, final TextView txtView, final ProgressBar spinner) {
        this.dialog = new ProgressDialog(context);
        this.fact = fact;
        this.bmImage = bmImage;
        this.txtView = txtView;
        this.spinner = spinner;
    }

    @Override
    protected void onPreExecute() {
        //this.dialog.setMessage("Loading some nature facts!");
        //this.dialog.show();
    }

    protected Bitmap doInBackground(String... urls) {
        String urlDisplay = urls[0];

        if (cache.containsKey(urlDisplay)) {
            return cache.get(urlDisplay);
        }

        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(urlDisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
            cache.put(urlDisplay, mIcon11);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
        txtView.setText(fact.getText());
        txtView.scrollTo(0, 0); // This is to scroll to the top if they had scrolled down previously
        spinner.setVisibility(View.GONE);
    }
}

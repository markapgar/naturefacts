package ninja.nature.naturefacts;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * Created by apgarm on 8/3/2017.
 */

public final class Playlist<T> {
    private ArrayList<T> internalList = new ArrayList<>();
    private int current = 0;

    public Playlist(final List<T> list) {
        internalList.addAll(list);
        current = 0;
    }

    /**
     * Shuffles the playlist.
     */
    public void shuffle() {
        Collections.shuffle(internalList);
    }

    /**
     * @return the current item in the list
     */
    public T getCurrent() {
        return internalList.get(current);
    }

    /**
     * Sets the current item to be the object found right after this current
     * in the list. If the item is the last one, it changes it to the first
     * item in the list
     *
     * @return the object right after the current item
     */
    public T getNext() {
        current++;
        if (current >= internalList.size()) {
            current = 0;
        }

        return getCurrent();
    }

    /**
     * Sets the current item to be the object found right before this current
     * in the list. If the item is the first one, it changes it to the last
     * item in the list
     *
     * @return the object right before the current item
     */
    public T getPrev() {
        current--;
        if (current < 0) {
            current = internalList.size() - 1;
        }

        return getCurrent();
    }
}

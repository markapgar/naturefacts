package ninja.nature.naturefacts;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.MobileAds;
import com.google.firebase.analytics.FirebaseAnalytics;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.GenericTypeIndicator;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class MainActivity extends AppCompatActivity {
    //private final String MY_URL_STRING = "http://nnnaturefacts20170718075323.azurewebsites.net/api/Facts/";
    //private final String MY_URL_STRING = "http://nnnaturefacts20170718075323.azurewebsites.net/api/NatureFacts/";

    //private final String AD_UNIT_ID = "ca-app-pub-1183858619009149/3457860872";

    private FirebaseAnalytics mFirebaseAnalytics;
    private GestureDetectorCompat mDetector;

    private ProgressBar spinner;
    private ImageButton btnReload;

    private Playlist<Fact> factPlaylist;
    private Playlist<Fact> stagedFactPlaylist;

    private boolean firstLoad = true;
    private NotificationCompat.Builder mBuilder;

    private boolean mIsInForegroundMode = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mFirebaseAnalytics = FirebaseAnalytics.getInstance(this);

        setContentView(R.layout.activity_main);

        spinner = (ProgressBar) findViewById(R.id.progressBar1);
        spinner.setVisibility(View.GONE);

        TextView textView = (TextView) findViewById(R.id.textView);
        textView.setMovementMethod(new ScrollingMovementMethod());

        //createTaskStartup("LOAD");
        spinner.setVisibility(View.VISIBLE);

        DatabaseReference databaseRef = FirebaseDatabase.getInstance().getReference("natureFacts");
        databaseRef.addValueEventListener(getValueEventListener());
        //databaseRef.addListenerForSingleValueEvent(getValueEventListener());
        //databaseRef.addChildEventListener(getChildEventListener());

        mBuilder =
                new NotificationCompat.Builder(this)
                        .setSmallIcon(R.drawable.ic_cloud_download_black_24dp)
                        .setContentTitle("Nature Facts")
                        .setContentText("New facts are available. Tap here to see them.");

        Intent resultIntent = new Intent(this, MainActivity.class);

        PendingIntent resultPendingIntent =
                PendingIntent.getActivity(
                        this,
                        0,
                        resultIntent,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );

        mBuilder.setContentIntent(resultPendingIntent);
        mBuilder.setAutoCancel(true);

        mDetector = new GestureDetectorCompat(this, new MyGestureListener());

        final ImageButton btnRandom = (ImageButton) findViewById(R.id.btnRandom);
        btnRandom.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (factPlaylist != null) {
                    factPlaylist.shuffle();
                    createTask(factPlaylist.getCurrent(), "RND");
                }
            }
        });


        final ImageButton btnNext = (ImageButton) findViewById(R.id.btnNext);
        btnNext.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (factPlaylist != null) {
                    createTask(factPlaylist.getNext(), "NXT");
                }
            }
        });

        final ImageButton btnPrev = (ImageButton) findViewById(R.id.btnPrev);
        btnPrev.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (factPlaylist != null) {
                    createTask(factPlaylist.getPrev(), "PRE");

                }
            }
        });

        btnReload = (ImageButton) findViewById(R.id.btnReload);
        btnReload.setVisibility(View.INVISIBLE);
        btnReload.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                factPlaylist = stagedFactPlaylist;
                btnReload.setVisibility(View.INVISIBLE);
                createTask(factPlaylist.getCurrent(), "RND");
            }
        });

        String APP_ID = "ca-app-pub-1183858619009149~1028150416";
        MobileAds.initialize(this, APP_ID);

        AdView mAdView;
        mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder()
                .addTestDevice("8B6B2711A9CE5356859B5140B5249E4C")
                .build();

        mAdView.loadAd(adRequest);

    }

    @Override
    protected void onPause() {
        super.onPause();
        mIsInForegroundMode = false;
    }

    @Override
    protected void onResume() {
        super.onResume();
        mIsInForegroundMode = true;
    }

    public boolean isInForeground() {
        return mIsInForegroundMode;
    }
//    private ChildEventListener getChildEventListener() {
//        return new ChildEventListener() {
//            private int timesIn = 0;
//            @Override
//            public void onChildAdded(DataSnapshot dataSnapshot, String s) {
//                timesIn++;
//                if(dataSnapshot != null){
//                    Log.i("INFO", "ChildEventListener:onChildAdded");
//                    Toast.makeText(MainActivity.this, "ChildEventListener:onChildAdded: " + timesIn, Toast.LENGTH_SHORT).show();
////                    Notification notification = dataSnapshot.getValue(Notification.class);
////
////                    showNotification(context,notification,dataSnapshot.getKey());
//                }
//            }
//
//            @Override
//            public void onChildChanged(DataSnapshot dataSnapshot, String s) {
//                Log.i("INFO", "ChildEventListener:onChildChanged");
//            }
//
//            @Override
//            public void onChildRemoved(DataSnapshot dataSnapshot) {
//                Log.i("INFO", "ChildEventListener:onChildRemoved");
//            }
//
//            @Override
//            public void onChildMoved(DataSnapshot dataSnapshot, String s) {
//                Log.i("INFO", "ChildEventListener:onChildMoved");
//            }
//
//            @Override
//            public void onCancelled(DatabaseError databaseError) {
//                Log.i("INFO", "ChildEventListener:onCancelled", databaseError.toException());
//            }
//        };
//    }

    private ValueEventListener getValueEventListener() {
        return new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {

                GenericTypeIndicator<ArrayList<Fact>> t = new GenericTypeIndicator<ArrayList<Fact>>() {
                };
                ArrayList<Fact> factList = dataSnapshot.getValue(t);
                Collections.sort(factList, new Comparator<Fact>() {
                    @Override
                    public int compare(Fact o1, Fact o2) {
                        return -o1.getId().compareTo(o2.getId());
                    }
                });
                stagedFactPlaylist = new Playlist<>(factList);
                if (firstLoad) {
                    factPlaylist = stagedFactPlaylist;
                    createTask(factPlaylist.getCurrent(), "RND");
                    firstLoad = false;
                } else {
                    if (isInForeground()){
                        btnReload.setVisibility(View.VISIBLE);
                        Toast.makeText(MainActivity.this, "New facts available. Press the cloud button in the top right to load new facts.", Toast.LENGTH_SHORT).show();

                    } else {
                        // Sets an ID for the notification
                        int mNotificationId = 001;
                        // Gets an instance of the NotificationManager service
                        NotificationManager mNotifyMgr =
                                (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
                        // Builds the notification and issues it.
                        mNotifyMgr.notify(mNotificationId, mBuilder.build());
                    }

                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {
                // Getting Post failed, log a message
                Log.w("INFO", "loadFact:onCancelled", databaseError.toException());
                // ...
            }
        };
    }




    @Override
    public boolean onTouchEvent(MotionEvent event) {
        this.mDetector.onTouchEvent(event);
        return super.onTouchEvent(event);
    }

    private void createTask(final Fact fact, final String action) {

        new AsyncTask<String, Void, String>() {
            @Override
            protected void onPreExecute() {
                spinner.setVisibility(View.VISIBLE);
            }

            @Override
            protected String doInBackground(String... params) {
                Log.d("MainActivity", action);
                return null;
            }

            @Override
            protected void onPostExecute(String res) {
                new BackgroundFactLoader(
                        MainActivity.this,
                        fact,
                        (ImageView) findViewById(R.id.imageView),
                        (TextView) findViewById(R.id.textView),
                        spinner
                ).execute(fact.getImageUrl());

                Bundle bundle = new Bundle();
                bundle.putString(FirebaseAnalytics.Param.ITEM_ID, fact.getId());
                bundle.putString(FirebaseAnalytics.Param.ITEM_NAME, "Fact");
                bundle.putString(FirebaseAnalytics.Param.CONTENT_TYPE, "image");
                bundle.putString(FirebaseAnalytics.Param.SEARCH_TERM, action);
                mFirebaseAnalytics.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, bundle);
            }

        }.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    }

    private class MyGestureListener extends GestureDetector.SimpleOnGestureListener {
        private static final String DEBUG_TAG = "Gestures";
        private static final int SWIPE_MIN_DISTANCE = 120;
        //private static final int SWIPE_MAX_OFF_PATH = 250;
        private static final int SWIPE_THRESHOLD_VELOCITY = 100;

        @Override
        public boolean onDown(MotionEvent event) {
            return true;
        }

//        @Override
//        public void onLongPress(MotionEvent event) {
//            Log.d(DEBUG_TAG, "onLongPress: " + event.toString());
//            if (factPlaylist != null) {
//                factPlaylist.shuffle();
//                createTask(factPlaylist.getCurrent(), "RND");
//            }
//        }

        @Override
        public boolean onFling(MotionEvent e1, MotionEvent e2,
                               float velocityX, float velocityY) {
            try {
//                //this is to detect how much y-axis movement there is
//                if (Math.abs(e1.getY() - e2.getY()) > SWIPE_MAX_OFF_PATH) {
//                    return false;
//                }

                if (e1.getX() - e2.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    // right to left swipe
                    Log.d(DEBUG_TAG, "rightToLeft");
                    if (factPlaylist != null) {
                        createTask(factPlaylist.getNext(), "NXT");
                    }
                    return true;
                } else if (e2.getX() - e1.getX() > SWIPE_MIN_DISTANCE
                        && Math.abs(velocityX) > SWIPE_THRESHOLD_VELOCITY) {
                    // left to right swipe
                    Log.d(DEBUG_TAG, "leftToRight");
                    if (factPlaylist != null) {
                        createTask(factPlaylist.getPrev(), "PRE");
                    }
                    return true;
                }
            } catch (Exception e) {
                // nothing
            }
            return false;
        }
    }
}
